class Profile < ActiveRecord::Base
  belongs_to :user
  validates :gender, presence: true,  inclusion: { in: %w(male female) }
  validate :first_or_last
  validate :not_allow

  def first_or_last
    if last_name.blank? && first_name.blank?
      errors.add(:base, "Specify at least one.")
    end
  end

  def not_allow
    if gender == "male" && first_name == "Sue"
      errors.add(:base, "does not allow a boy named Sue")
    end
  end

  def self.get_all_profiles(min_birth_year, max_birth_year)
    Profile.where(:birth_year => min_birth_year..max_birth_year).order('birth_year ASC')
  end
end
